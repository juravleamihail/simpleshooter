// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"
#include "Gun.h"
#include "Components/CapsuleComponent.h"
#include "SimpleShooterGameModeBase.h"

// Sets default values
AShooterCharacter::AShooterCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	Health = MaxHealth;

	InitWeapons();

	SelectWeapon(currentWeapon);
}

bool AShooterCharacter::IsDead() const
{
	return Health <= 0;
}

float AShooterCharacter::GetHealthPercent() const
{
	return Health / MaxHealth;
}

// Called every frame
void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AShooterCharacter::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &AShooterCharacter::LookUpRate);
	PlayerInputComponent->BindAxis(TEXT("LookRightRate"), this, &AShooterCharacter::LookRightRate);
	PlayerInputComponent->BindAction(TEXT("Shoot"), EInputEvent::IE_Pressed, this, &AShooterCharacter::Shoot);
	PlayerInputComponent->BindAction(TEXT("SwitchWeapon"), EInputEvent::IE_Pressed, this, &AShooterCharacter::SwitchWeapon);
}

void AShooterCharacter::MoveForward(float AxisValue)
{
	//UE_LOG(LogTemp, Warning, TEXT("Axis Value: %f"), AxisValue);

	if (AxisValue >= 0.5f || AxisValue <= -0.5f)
	{
		AddMovementInput(GetActorForwardVector() * AxisValue);
	}
}

void AShooterCharacter::MoveRight(float AxisValue)
{
	if (AxisValue > 0.5f || AxisValue < -0.5f)
	{
		AddMovementInput(GetActorRightVector() * AxisValue);
	}
}

void AShooterCharacter::LookUpRate(float AxisValue)
{
	if (AxisValue >= 0.5f || AxisValue <= -0.5f)
	{
		AddControllerPitchInput(AxisValue * RotationRate * GetWorld()->GetDeltaSeconds());
	}
}

void AShooterCharacter::LookRightRate(float AxisValue)
{
	if (AxisValue >= 0.5f || AxisValue <= -0.5f)
	{
		AddControllerYawInput(AxisValue * RotationRate * GetWorld()->GetDeltaSeconds());
	}
}

void AShooterCharacter::Shoot()
{
	AGun* CurrentWeapon = GetCurrentWeapon();

	if (CurrentWeapon != nullptr)
	{
		GetCurrentWeapon()->PullTrigger();
	}
}

AGun* AShooterCharacter::GetCurrentWeapon()
{
	switch (currentWeapon)
	{
	case WeaponType::Rifle:
		return Rifle;
		break;
	case WeaponType::Launcher:
		return Launcher;
		break;
	default:
		break;
	}

	return nullptr;
}

AActor* AShooterCharacter::GetGun()
{
	return GetCurrentWeapon();
}

float AShooterCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float DamageToApply = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	DamageToApply = FMath::Min(Health, DamageToApply);
	Health -= DamageToApply;

	if (IsDead())
	{
		ASimpleShooterGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASimpleShooterGameModeBase>();

		if (GameMode != nullptr)
		{
			GameMode->PawnKilled(this);
		}

		DetachFromControllerPendingDestroy();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	UE_LOG(LogTemp, Warning, TEXT("%f  Health left"), Health);
	return DamageToApply;
}

void AShooterCharacter::SwitchWeapon()
{
	if (currentWeapon == WeaponType::Rifle)
	{
		currentWeapon = WeaponType::Launcher;
	}
	else
	{
		currentWeapon = WeaponType::Rifle;
	}

	SelectWeapon(currentWeapon);
}

void AShooterCharacter::SelectWeapon(WeaponType weaponType)
{
	switch (weaponType)
	{
	case WeaponType::Rifle:
		SetActive(Rifle, true);
		SetActive(Launcher, false);
		break;
	case WeaponType::Launcher:
		SetActive(Launcher, true);
		SetActive(Rifle, false);
		break;
	default:
		break;
	}
}

void AShooterCharacter::InitWeapons()
{
	AddRifle();
	AddLauncher();
	DisableWeapons();
}

void AShooterCharacter::DisableWeapons()
{
	SetActive(Rifle, false);
	SetActive(Launcher, false);
}

void AShooterCharacter::AddRifle()
{
	Rifle = GetWorld()->SpawnActor<AGun>(RifleClass);
	GetMesh()->HideBoneByName(TEXT("weapon_r"), EPhysBodyOp::PBO_None);
	Rifle->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform,
		TEXT("WeaponSocket"));
	Rifle->SetOwner(this);
}

void AShooterCharacter::AddLauncher()
{
	Launcher = GetWorld()->SpawnActor<AGun>(LauncherClass);
	GetMesh()->HideBoneByName(TEXT("weapon_r"), EPhysBodyOp::PBO_None);
	Launcher->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform,
		TEXT("WeaponSocket"));
	Launcher->SetOwner(this);
}

void AShooterCharacter::SetActive(AActor* Actor, bool Active)
{
	Actor->SetActorTickEnabled(Active);
	Actor->SetActorHiddenInGame(Active);
	Actor->SetActorEnableCollision(Active);
}
