// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/SkinnedMeshComponent.h"
#include "ShooterCharacter.generated.h"

class AGun;

UENUM()
enum WeaponType
{
	Rifle     UMETA(DisplayName = "Rifle"),
	Launcher      UMETA(DisplayName = "Launcher"),
};

UCLASS()
class SIMPLESHOOTER_API AShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AShooterCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	UFUNCTION(BlueprintPure)
	bool IsDead() const;

	UFUNCTION(BlueprintPure)
	float GetHealthPercent() const;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void Shoot();

	UFUNCTION()
	AGun* GetCurrentWeapon();

	UFUNCTION(BlueprintCallable)
	AActor* GetGun();

private:
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void LookUpRate(float AxisValue);
	void LookRightRate(float AxisValue);

	UPROPERTY(EditAnywhere)
	float RotationRate = 70;

	UPROPERTY(EditDefaultsOnly)
	float MaxHealth;

	UPROPERTY(VisibleAnywhere)
	float Health;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AGun> RifleClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AGun> LauncherClass;

	UPROPERTY()
	AGun* Rifle;

	UPROPERTY()
	AGun* Launcher;

	UPROPERTY()
	TArray<AGun*> Weapons;

	UPROPERTY()
	TEnumAsByte<WeaponType> currentWeapon = WeaponType::Rifle;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	void SwitchWeapon();

	void SelectWeapon(WeaponType weaponType);

	void InitWeapons();

	void DisableWeapons();

	void AddRifle();

	void AddLauncher();

	void SetActive(AActor* Actor, bool Active);

};
